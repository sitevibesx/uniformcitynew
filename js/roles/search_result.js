/*
a method to verify that search result is clicked.  

this was needed for cases where search results are asynchronously loaded via js.  we need to delegate our event handling
in these scenarios.  Bsically, this method should verify that a result item was clicked and then return the url that 
the browser is going to.  Otherwise it should return false.  

Receives one argument, a reference to the event target node.

*/

SiteVibes.searchResultClickVerifier = function(el){
    var ref = this;

    //for testing page, the easiest way to tell if a search result was clicked based on the node is to see if 
    // either the element, the element's parent, or the element's parent's parent have the class 'item-item'

    if(el.className &&
       el.className.match(/\s*item-item\s*/)){
        return el.querySelector('a').href;
    }

    if(el.parentNode && 
       el.parentNode.className &&
       el.parentNode.className.match(/\s*item-item\s*/)){
        return el.parentNode.querySelector('a').href;
    }

    if(el.parentNode && 
       el.parentNode.parentNode && 
       el.parentNode.parentNode.className &&
       el.parentNode.parentNode.className.match(/\s*item-item\s*/)){
        return el.parentNode.parentNode.querySelector('a').href;
    }

    return false;
}

//a method that returns whatever the search terms used to get to the current page are
// the terms are usually in the current url or at the very least somewhere on the page

SiteVibes.searchResultExtractSearchTerm = function(){
    var regex = /searchPage.html\?keyword=([^&]+)/

    var term = regex.exec(document.location.href);

    if(typeof(term) == 'object' &&
       term != null &&
       term[1]){
        return term[1];
    }

    return '';
}

//a hook to do stuff on this roles start.  default is to do nothing
SiteVibes.startSearchResultHook = function(){
    return;
}


